from classes.items.ItemHealthPotion import ItemHealthPotion
from classes.items.ItemShuriken import ItemShuriken

# number, name, image_asset, coordinates, font_name, max_health, damage, loot
NPC_0 = (0, "Ass", "assets/npc/test.png", 'sans', 3, 3, ItemHealthPotion())

NPC_NINJA_SHURIKEN = (1, "Ninja", "assets/npc/ninja_shuriken.png", 'sans', 3, 1, ItemShuriken())
NPC_NINJA_KATANA = (2, "Ninja", "assets/npc/ninja_katana.png", 'sans', 3, 3, ItemShuriken())

NPC_PIRATE_CAPTAIN = (3, "Капитан", "assets/npc/pirate_captain.png", 'sans', 4, 2, ItemHealthPotion())
NPC_PIRATE_FAT = (4, "Толстяк", "assets/npc/pirate_fat.png", 'sans', 5, 1, ItemHealthPotion())
NPC_PIRATE_OLD = (3, "Пажилой павук", "assets/npc/pirate_old.png", 'sans', 2, 1, ItemHealthPotion())
