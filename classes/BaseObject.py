import pygame

from classes.RectObject import RectObject


class BaseObject(RectObject):
    # Название
    name: str
    # Спрайт
    image: pygame.sprite.Sprite

    def __init__(self, name, image, coordinates, size):
        super().__init__(coordinates, size)
        self.name = name
        self.image = image
