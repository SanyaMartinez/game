import pygame

from classes.RectObject import RectObject


class Button(RectObject):

    font: pygame.font
    title_text: str
    title: pygame.Surface

    def __init__(self, coordinates, size, title_text, func, *args):
        super().__init__(coordinates, size)
        self.font = pygame.font.SysFont(None, 30)
        self.title_text = title_text
        self.func = func
        self.parameters = args
        self.create_title()

    def create_title(self):
        self.title = self.font.render(self.title_text, True, (0, 0, 0))

    def on_click(self):
        return self.func(*self.parameters)
