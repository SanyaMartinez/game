from enum import Enum


# Направления передвижения игрока
class Direction(Enum):
    up = 1
    left = 2
    down = 3
    right = 4
