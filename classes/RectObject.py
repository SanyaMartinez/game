import pygame


class RectObject(pygame.sprite.Sprite):
    # Хитбокс
    rect: pygame.Rect

    def __init__(self, coordinates, size):
        super().__init__()
        self.rect = pygame.Rect(coordinates, size)
