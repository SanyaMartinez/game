import pygame

from classes.battle.SelectedAction import SelectedAction
from classes.battle.Stage import Stage
from classes.items.ItemHealthPotion import ItemHealthPotion
from classes.items.ItemWeakPotion import ItemWeakPotion
from classes.items.ItemShuriken import ItemShuriken
from classes.items.ItemStrengthPotion import ItemStrengthPotion
from classes.battle.BattleInterface import BattleInterface
from classes.npc.NPCInfo import NPCInfo
from classes.player.PlayerHeart import PlayerHeart
from classes.player.PlayerInfo import PlayerInfo
from constants import *
from damage_objects_info import create_battle_damage_objects


class Battle:
    # Основные параметры для инициализации сражения
    number: int
    background: pygame.image
    surface: pygame.Surface
    stage: Stage
    selected_action: SelectedAction

    # Меню - кнопки и информация
    interface: BattleInterface

    # Информация об игроке и противнике
    player_info: PlayerInfo
    npc_info: NPCInfo

    # Бой
    fight_start_time_ticks: int
    current_duration: int
    max_duration: int
    player_heart: PlayerHeart
    player_start_coordinates: tuple
    damage_objects: pygame.sprite.Group

    def __init__(self, number, player_info, npc_info):
        self.start_action_selection()
        self.selected_action = SelectedAction.press_fight
        self.background = pygame.transform.scale(pygame.image.load(BATTLE_BACKGROUND), SCREEN_SIZE)
        self.surface = pygame.Surface(self.background.get_rect().size)
        self.number = number
        self.player_start_coordinates = PLAYER_HEART_START_COORDINATES
        self.player_info = player_info
        self.npc_info = npc_info
        self.interface = BattleInterface(self.surface, self.player_info, self.npc_info)

    def draw(self):
        self.surface.blit(self.background, (0, 0))
        self.interface.draw(self.stage)
        if self.stage == Stage.fighting:
            self.damage_objects.draw(self.surface)
            self.surface.blit(self.player_heart.image, self.player_heart.rect)

    def start_action_selection(self):
        self.stage = Stage.action_selection

    def start_fight(self):
        self.create_player_heart()
        self.create_damage_objects()
        self.stage = Stage.fighting
        # Начало отсчета длительности раунда  TODO перенести?
        self.fight_start_time_ticks = pygame.time.get_ticks()
        self.max_duration = 10

    def stop_fight(self):
        self.check_health()
        if self.npc_info.current_health > 0 and self.player_info.current_health > 0:
            self.start_action_selection()

    def check_health(self):
        if self.npc_info.current_health <= 0:
            self.stage = Stage.win
        elif self.player_info.current_health <= 0:
            self.stage = Stage.lose

    def check_time_for_end(self):
        self.current_duration = (pygame.time.get_ticks() - self.fight_start_time_ticks) / 1000
        if self.current_duration > self.max_duration:
            # Раунд в пользу игрока
            self.npc_info.current_health -= self.player_info.damage
            self.interface.update()
            self.stop_fight()

    # Обновление позиции игрока и камеры (движение)
    def update_position(self):
        self.player_heart.move()
        for obj in self.damage_objects:
            obj.move(self.player_heart.rect, self.damage_objects)

        damage_taken = self.player_heart.check_damage_taken(self.damage_objects)
        if damage_taken:
            # Раунд в пользу противника
            self.player_info.current_health -= self.npc_info.damage
            self.interface.update()
            self.stop_fight()

    # Создаем игрока
    def create_player_heart(self):
        name = self.player_info.name
        image = pygame.image.load(PLAYER_HEART_SKIN).convert_alpha()
        size = (PLAYER_HEART_SIZE, PLAYER_HEART_SIZE)
        self.player_heart = PlayerHeart(name, image, self.player_start_coordinates, size, self.player_info)

    # Создаем атакующие объекты
    def create_damage_objects(self):
        self.damage_objects = create_battle_damage_objects(self.number)

    def start_item_selection(self):
        self.stage = Stage.item_selection

    def start_item_using(self):
        self.stage = Stage.item_using

    def use_item(self):
        item = self.interface.selected_item

        if type(item) is ItemHealthPotion:
            if self.player_info.current_health == self.player_info.max_health:
                self.player_info.max_health += item.effect_value
            self.player_info.current_health += item.effect_value
        elif type(item) is ItemStrengthPotion:
            self.player_info.damage += item.effect_value
        elif type(item) is ItemShuriken:
            self.npc_info.current_health -= item.effect_value
        elif type(item) is ItemWeakPotion:
            self.npc_info.damage -= item.effect_value

        self.interface.update()
        self.stage = Stage.item_selection
        self.check_health()

    def start_mercy_selection(self):
        self.stage = Stage.mercy_selection
