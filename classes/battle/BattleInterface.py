import pygame

from classes.Button import Button
from classes.battle.SelectedAction import SelectedAction
from classes.battle.Stage import Stage
from classes.items.BaseItem import BaseItem
from classes.npc.NPCInfo import NPCInfo
from classes.player.PlayerInfo import PlayerInfo
from classes.text.BattleTextWindow import BattleTextWindow
from constants import BATTLE_BUTTON_COORDINATES_1, \
    BATTLE_BUTTON_COORDINATES_2, BATTLE_BUTTON_COORDINATES_3, BATTLE_BUTTON_SIZE, \
    BATTLE_PLAYER_INFO_TEXT_COORDINATES, BATTLE_NPC_INFO_TEXT_COORDINATES, BATTLE_ITEM_BUTTON_SIZE, \
    BATTLE_ITEM_BUTTON_COORDINATES


class BattleInterface:

    # Текст с информацией о сражении
    text_window: BattleTextWindow

    # Кнопки снизу
    buttons: pygame.sprite.Group
    surface: pygame.Surface
    click: bool

    # Информация об игроке и противнике
    player_info: PlayerInfo
    npc_info: NPCInfo

    # Текст с информацией об игроке и противнике
    font: pygame.font
    player_info_text: pygame.Surface
    npc_info_text: pygame.Surface

    # Использованный предмет из инвенторя
    selected_item: BaseItem

    def __init__(self, surface, player_info, npc_info):
        self.font = pygame.font.SysFont(None, 30)
        self.surface = surface
        self.click = False
        self.set_screen(self.get_buttons_action_selection())
        self.text_window = BattleTextWindow(self.surface)
        self.player_info = player_info
        self.npc_info = npc_info
        self.update()

    def update(self):
        self.update_player_info_text()
        self.update_npc_info_text()

    def update_player_info_text(self):
        name = self.player_info.name
        current_health = self.player_info.current_health
        max_health = self.player_info.max_health
        damage = self.player_info.damage
        text = "player: {}  |  health: {} / {}  |  damage: {}".format(name, current_health, max_health, damage)
        self.font = pygame.font.SysFont(None, 30)
        self.player_info_text = self.font.render(text, True, (255, 255, 255))

    def update_npc_info_text(self):
        name = self.npc_info.name
        current_health = self.npc_info.current_health
        max_health = self.npc_info.max_health
        damage = self.npc_info.damage
        text = "enemy: {}  |  health: {} / {}  |  damage: {}".format(name, current_health, max_health, damage)
        self.font = pygame.font.SysFont(None, 30)
        self.npc_info_text = self.font.render(text, True, (255, 255, 255))

    def draw_characters_text(self):
        player_text_rect = self.player_info_text.get_rect()
        player_text_rect.topleft = BATTLE_PLAYER_INFO_TEXT_COORDINATES
        self.surface.blit(self.player_info_text, player_text_rect)

        npc_text_rect = self.npc_info_text.get_rect()
        npc_text_rect.topright = BATTLE_NPC_INFO_TEXT_COORDINATES
        self.surface.blit(self.npc_info_text, npc_text_rect)

    def draw_info_text(self, text):
        self.text_window.blit_text(text)

    def set_screen(self, buttons):
        self.buttons = buttons

    @staticmethod
    def create_buttons(buttons_list):
        buttons = pygame.sprite.Group()
        for button in buttons_list:
            buttons.add(button)
        return buttons

    def get_buttons_action_selection(self):
        button1 = Button(BATTLE_BUTTON_COORDINATES_1, BATTLE_BUTTON_SIZE, "Битва", self.press_fight)
        button2 = Button(BATTLE_BUTTON_COORDINATES_2, BATTLE_BUTTON_SIZE, "Вещи", self.press_items)
        button3 = Button(BATTLE_BUTTON_COORDINATES_3, BATTLE_BUTTON_SIZE, "Пощада", self.press_mercy)
        return self.create_buttons([button1, button2, button3])

    def get_buttons_item_selection(self):
        button1 = Button(BATTLE_BUTTON_COORDINATES_1, BATTLE_BUTTON_SIZE, "Назад", self.perform_action_selection)
        all_buttons = [button1]

        button_start_pos_x, button_start_pos_y = BATTLE_ITEM_BUTTON_COORDINATES
        button_pos_x, button_pos_y = button_start_pos_x, button_start_pos_y
        inventory_items = self.player_info.inventory
        # счетчик
        item_number = 0
        for item in inventory_items:
            item_button = Button((button_pos_x, button_pos_y), BATTLE_ITEM_BUTTON_SIZE,
                                 item.name, self.press_inventory_item, item)
            all_buttons.append(item_button)
            # координаты отрисовки кнопки
            item_number += 1
            if item_number % 3 == 0:
                button_pos_x += 220
                button_pos_y = button_start_pos_y
            else:
                button_pos_y += 50

        return self.create_buttons(all_buttons)

    def get_buttons_item_use(self):
        button1 = Button(BATTLE_BUTTON_COORDINATES_1, BATTLE_BUTTON_SIZE, "Назад", self.press_items)
        button2 = Button(BATTLE_BUTTON_COORDINATES_3, BATTLE_BUTTON_SIZE, "Использовать", self.press_use_item)
        return self.create_buttons([button1, button2])

    def get_buttons_mercy_selection(self):
        button1 = Button(BATTLE_BUTTON_COORDINATES_1, BATTLE_BUTTON_SIZE, "Назад", self.perform_action_selection)
        button2 = Button(BATTLE_BUTTON_COORDINATES_3, BATTLE_BUTTON_SIZE, "Сбежать", self.press_run_away)
        return self.create_buttons([button1, button2])

#####
    # Выбор действия по нажатию на кнопки
    def perform_action_selection(self):
        self.set_screen(self.create_buttons(self.get_buttons_action_selection()))
        return SelectedAction.press_back_to_action_selection

    @staticmethod
    def press_fight():
        return SelectedAction.press_fight

    def press_items(self):
        self.set_screen(self.get_buttons_item_selection())
        return SelectedAction.press_items

    def press_inventory_item(self, item):
        self.selected_item = item
        self.set_screen(self.get_buttons_item_use())
        return SelectedAction.press_inventory_item

    def press_use_item(self):
        self.player_info.inventory.remove(self.selected_item)
        self.set_screen(self.get_buttons_item_selection())
        return SelectedAction.press_use_item

    def press_mercy(self):
        self.set_screen(self.get_buttons_mercy_selection())
        return SelectedAction.press_mercy

    @staticmethod
    def press_run_away():
        return SelectedAction.press_run_away
#####

    def draw(self, stage):
        self.draw_characters_text()

        if stage == Stage.action_selection:
            self.draw_info_text("Готовьтесь к бою c {}!\nАтакуйте противника, "
                                "используйте снаряжение или бегите прочь..".format(self.npc_info.name))
        elif stage == Stage.item_selection:
            self.draw_info_text("Выберите предмет из инвентаря")
        elif stage == Stage.item_using:
            name = self.selected_item.name
            description = self.selected_item.description
            self.draw_info_text("{}, {}\n\n"
                                "Вы действительно хотите использовать этот предмет?".format(name, description))
        elif stage == Stage.mercy_selection:
            self.draw_info_text("Вы хотите сбежать из боя?")

        if stage != Stage.fighting:
            self.draw_buttons()

    def draw_buttons(self):
        for button in self.buttons:
            # Фон кнопки
            pygame.draw.rect(self.surface, (255, 255, 255), button)
            # Текст кнопки
            title_rect = button.title.get_rect()
            title_rect.center = button.rect.center
            self.surface.blit(button.title, title_rect)

    def check_click(self):
        mouse_pos = pygame.mouse.get_pos()

        for button in self.buttons:
            if button.rect.collidepoint(mouse_pos):
                if self.click:
                    self.click = False
                    return button.on_click()

        self.click = False
