from enum import Enum


class SelectedAction(Enum):
    # Нажали "Битва"
    press_fight = 1
    # Нажали "Вещи
    press_items = 2
    # Нажали "Пощада"
    press_mercy = 3

    # Нажали на предмет в инвентаре
    press_inventory_item = 4
    # Нажали использовать
    press_use_item = 5

    # Нажали сбежать
    press_run_away = 6

    # Нажали "Назад" и перешли к выбору действий
    press_back_to_action_selection = 7
