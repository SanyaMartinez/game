from enum import Enum


class Stage(Enum):
    # Начальный выбор действия
    action_selection = 1
    # Сражение
    fighting = 2
    # Выбор предмета инвентаря
    item_selection = 3
    # Использование предмета
    item_using = 4
    # Выбор пощады
    mercy_selection = 5
    # Победа в сражении
    win = 6
    # Поражение в сражении
    lose = 7
