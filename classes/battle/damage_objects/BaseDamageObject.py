from classes.BaseObject import BaseObject


class BaseDamageObject(BaseObject):
    speed: float

    def __init__(self, name, image, coordinates, size, speed):
        super().__init__(name, image, coordinates, size)
        self.speed = speed

    def move(self, *args):
        pass
