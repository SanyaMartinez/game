import random

import pygame

from classes.battle.damage_objects.BaseDamageObject import BaseDamageObject
from constants import FIGHT_BORDER_X_LEFT, FIGHT_BORDER_X_RIGHT, FIGHT_BORDER_Y_TOP, FIGHT_BORDER_Y_BOTTOM


class Explosion(BaseDamageObject):

    # Задержка перед взрывом
    default_delay: int
    delay: int

    # Начальные значения
    default_image: pygame.image
    start_coordinates: tuple

    increase: bool

    def __init__(self, name, image, coordinates, size, speed, delay):
        super().__init__(name, image, coordinates, size, speed)
        self.default_delay = delay
        self.delay = delay
        # Доп. параметры
        self.change_random_coordinates()
        self.default_image = image
        self.image = pygame.transform.scale(image, size)
        self.increase = True

    def move(self, *args):
        # Задержка взрыва
        if self.delay > 0:
            self.delay -= 1
            return

        if self.increase:
            change_of_size = +self.speed
        else:
            change_of_size = -self.speed

        self.rect.width += change_of_size
        self.rect.height += change_of_size

        self.image = pygame.transform.smoothscale(self.default_image, self.rect.size)
        self.rect.center = self.start_coordinates

        if self.rect.width > 300:
            self.increase = False
        if self.rect.width <= self.speed:
            self.change_random_coordinates()
            self.increase = True
            self.delay = self.default_delay

    def change_random_coordinates(self):
        coordinates = (random.randint(FIGHT_BORDER_X_LEFT, FIGHT_BORDER_X_RIGHT),
                       random.randrange(FIGHT_BORDER_Y_TOP, FIGHT_BORDER_Y_BOTTOM))
        self.rect.center = coordinates
        self.start_coordinates = coordinates
