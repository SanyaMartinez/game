from classes.battle.damage_objects.BaseDamageObject import BaseDamageObject


class Katana(BaseDamageObject):

    def __init__(self, name, image, coordinates, size, speed):
        super().__init__(name, image, coordinates, size, speed)

    def move(self, *args):
        self.rect.x += self.speed
