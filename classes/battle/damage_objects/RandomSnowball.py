import random
from classes.battle.damage_objects.BaseDamageObject import BaseDamageObject
from constants import *


class RandomSnowball(BaseDamageObject):

    def __init__(self, name, image, coordinates, size, speed):
        super().__init__(name, image, coordinates, size, speed)

    def move(self, *args):
        x_step = random.randint(0, 1) * self.speed * random.randint(-3, 3)
        y_step = random.randint(0, 1) * self.speed * random.randint(-2, 2)

        if FIGHT_BORDER_X_LEFT < self.rect.x + x_step < FIGHT_BORDER_X_RIGHT:
            self.rect.x += x_step
        if FIGHT_BORDER_Y_TOP < self.rect.y + y_step < FIGHT_BORDER_Y_BOTTOM:
            self.rect.y += y_step
