import random
from classes.battle.damage_objects.BaseDamageObject import BaseDamageObject
from constants import *


class Shuriken(BaseDamageObject):

    def __init__(self, name, image, coordinates, size, speed):
        super().__init__(name, image, coordinates, size, speed)

    def move(self, player_position, objects):
        # Убираем, чтобы не считало столкновения с самим собой
        objects.remove(self)

        # Движение по x
        if self.rect.x < player_position.x:
            x_step = self.speed * random.randint(0, 3)
        else:
            x_step = -self.speed * random.randint(0, 3)

        new_rect = self.rect.copy()
        new_rect.x += x_step

        if FIGHT_BORDER_X_LEFT < new_rect.x < FIGHT_BORDER_X_RIGHT and self.check_collide(new_rect, objects):
            self.rect = new_rect

        # Движение по y
        if self.rect.y < player_position.y:
            y_step = random.randint(0, 1) * self.speed * random.randint(0, 3)
        else:
            y_step = random.randint(0, 1) * -self.speed * random.randint(0, 3)

        new_rect = self.rect.copy()
        new_rect.y += y_step

        if FIGHT_BORDER_Y_TOP < self.rect.y + y_step < FIGHT_BORDER_Y_BOTTOM and self.check_collide(new_rect, objects):
            self.rect = new_rect

        # Возвращаем обратно
        objects.add(self)

    @staticmethod
    def check_collide(rect, objects):
        for single_object in objects:
            if rect.colliderect(single_object.rect):
                return False
        return True
