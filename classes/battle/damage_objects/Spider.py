from classes.battle.damage_objects.BaseDamageObject import BaseDamageObject


class Spider(BaseDamageObject):

    def __init__(self, name, image, coordinates, size, speed):
        super().__init__(name, image, coordinates, size, speed)

    def move(self, *args):
        self.rect.y += self.speed
