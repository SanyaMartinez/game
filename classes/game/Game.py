from classes.battle.SelectedAction import SelectedAction
from classes.game.GameResult import GameResult
from classes.game.GameStatus import GameStatus
from classes.level.Level import Level
from classes.battle.Battle import Stage, Battle
from classes.text.DialogWindow import DialogWindow
from classes.menu.MainMenu import MainMenu
from classes.menu.PauseMenu import PauseMenu
from classes.player.PlayerInfo import PlayerInfo
from constants import *
from classes.player.Player import Direction
import pygame

from levels_info import ALL_LEVELS


class Game:
    is_running: bool

    player_info: PlayerInfo

    main_menu: MainMenu
    pause_menu: PauseMenu
    status: GameStatus
    level: Level
    battle: Battle
    dialog_window: DialogWindow

    prev_status: GameStatus  # для паузы

    screen: pygame.display
    clock: pygame.time.Clock

    # Инициализация игры
    def __init__(self):
        # Жизненный цикл игры
        self.is_running = True

        # Настройки экрана
        pygame.init()
        pygame.display.set_caption(SCREEN_TITLE)
        self.screen = pygame.display.set_mode(SCREEN_SIZE)
        self.clock = pygame.time.Clock()

        # Инициализируем главное меню
        self.main_menu = MainMenu(self.screen)

        # Инициализируем диалоговое окно
        self.dialog_window = DialogWindow(self.screen)

        # Начальный статус игры
        self.status = GameStatus.main_menu

    def create_player(self, difficulty):
        max_health = difficulty.value
        self.player_info = PlayerInfo("sonya", max_health)

    def set_level(self, number, player_start_coordinates):
        self.level = ALL_LEVELS[number]
        self.level.start_level(self.player_info, player_start_coordinates)

    def check_battle(self, npc):
        self.battle = Battle(npc.number, self.player_info, npc.npc_info)
        self.status = GameStatus.battle

    def play(self):
        while self.is_running:
            if self.status == GameStatus.main_menu:
                self.perform_main_menu()
            elif self.status == GameStatus.pause_menu:
                self.perform_pause_menu()
            elif self.status == GameStatus.moving:
                self.perform_moving()
            elif self.status == GameStatus.battle:
                self.perform_battle()
            elif self.status == GameStatus.dialog:
                self.perform_dialog()
            self.clock.tick(FPS)
            pygame.display.flip()

        # Закрываем игру
        pygame.quit()

    def check_pause_and_exit(self, event):
        # Выход из игры - Esc / Крестик
        if event.type == pygame.QUIT:
            self.is_running = False
        # Пауза - Esc
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            self.level.player.stop_move()
            # Инициализируем меню паузы
            self.pause_menu = PauseMenu(self.screen)
            self.prev_status = self.status
            self.status = GameStatus.pause_menu

    def perform_main_menu(self):
        # Отрисовка меню
        self.screen.fill(SCREEN_BLACK)
        self.main_menu.draw()
        self.main_menu.check_click()

        if self.main_menu.exit:
            self.is_running = False
        if self.main_menu.start:
            self.start()

        for event in pygame.event.get():
            self.handle_event_while_main_menu(event)

    def handle_event_while_main_menu(self, event):
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            self.is_running = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                self.main_menu.click = True

    def start(self):
        self.create_player(self.main_menu.difficulty)
        self.dialog_window.set_player_name(self.player_info.name)
        self.set_level(0, GAME_START_PLAYER_COORDINATES)
        self.status = GameStatus.moving

    def perform_pause_menu(self):
        # Отрисовка меню
        self.pause_menu.draw()
        self.pause_menu.check_click()

        if self.pause_menu.exit:
            self.is_running = False
        if self.pause_menu.resume:
            self.resume_game()

        for event in pygame.event.get():
            self.handle_event_while_pause_menu(event)

    def handle_event_while_pause_menu(self, event):
        if event.type == pygame.QUIT:
            self.is_running = False
        if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            self.resume_game()
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                self.pause_menu.click = True

    def resume_game(self):
        self.status = self.prev_status

    def perform_moving(self):
        # Движение игрока и камеры
        self.level.update_position()

        # Отрисовываем уровень и все объекты
        self.level.draw()
        self.screen.fill(SCREEN_BLACK)
        self.screen.blit(self.level.surface, self.level.camera_pos)

        for event in pygame.event.get():
            self.handle_event_while_moving(event)

    def handle_event_while_moving(self, event):
        self.check_pause_and_exit(event)
        if event.type == pygame.KEYDOWN:
            key = event.key
            # Начинаем передвижение игрока
            if key == pygame.K_w or key == pygame.K_UP:
                self.level.player.change_move_to(Direction.up)
            elif key == pygame.K_a or key == pygame.K_LEFT:
                self.level.player.change_move_to(Direction.left)
            elif key == pygame.K_s or key == pygame.K_DOWN:
                self.level.player.change_move_to(Direction.down)
            elif key == pygame.K_d or key == pygame.K_RIGHT:
                self.level.player.change_move_to(Direction.right)
            elif key == pygame.K_RETURN:
                # Начинаем диалог с NPC
                npc = self.level.check_npc_nearby()
                if npc is not None:
                    if npc.dialog_texts is not None:
                        self.dialog_window.set_dialog_preferences(npc.font_name, npc.dialog_texts, npc.name)
                        self.status = GameStatus.dialog

                # Подбираем лут
                self.level.check_loot_nearby()

                # Начинаем переход на другой уровень
                next_level_number, player_start_coordinates = self.level.check_level_transition()
                if next_level_number is not None:
                    self.set_level(next_level_number, player_start_coordinates)

            # TODO rework start battle?
            elif key == pygame.K_b:
                # Начинаем сражение с NPC
                npc = self.level.check_npc_nearby()
                if npc is not None:
                    self.check_battle(npc)

        if event.type == pygame.KEYUP:
            key = event.key
            # Останавливаем передвижение игрока
            if key == pygame.K_w or key == pygame.K_UP:
                self.level.player.change_move_to(Direction.up)
            elif key == pygame.K_a or key == pygame.K_LEFT:
                self.level.player.change_move_to(Direction.left)
            elif key == pygame.K_s or key == pygame.K_DOWN:
                self.level.player.change_move_to(Direction.down)
            elif key == pygame.K_d or key == pygame.K_RIGHT:
                self.level.player.change_move_to(Direction.right)

    def perform_battle(self):
        # Отрисовываем bg боя
        self.battle.draw()
        self.screen.fill(SCREEN_BLACK)
        self.screen.blit(self.battle.surface, (0, 0))

        # Выбор действия
        if self.battle.stage == Stage.action_selection:
            selected_action = self.battle.interface.check_click()
            if selected_action == SelectedAction.press_fight:
                self.battle.start_fight()
            elif selected_action == SelectedAction.press_items:
                self.battle.start_item_selection()
            elif selected_action == SelectedAction.press_mercy:
                self.battle.start_mercy_selection()

            for event in pygame.event.get():
                self.handle_event_while_battle_selection(event)

        # Атака, уклонение
        elif self.battle.stage == Stage.fighting:
            # Движение игрока и камеры
            self.battle.update_position()
            # Таймер раунда боя
            self.battle.check_time_for_end()

            for event in pygame.event.get():
                self.handle_event_while_battle_fighting(event)

        # Выбор предмета из инвентаря
        elif self.battle.stage == Stage.item_selection:
            selected_action = self.battle.interface.check_click()
            if selected_action == SelectedAction.press_back_to_action_selection:
                self.battle.start_action_selection()
            elif selected_action == SelectedAction.press_inventory_item:
                self.battle.start_item_using()

            for event in pygame.event.get():
                self.handle_event_while_battle_selection(event)

        # Использование предмета
        elif self.battle.stage == Stage.item_using:
            selected_action = self.battle.interface.check_click()
            if selected_action == SelectedAction.press_items:
                self.battle.start_item_selection()
            elif selected_action == SelectedAction.press_use_item:
                self.battle.use_item()

            for event in pygame.event.get():
                self.handle_event_while_battle_selection(event)

        # Сбежать из боя
        elif self.battle.stage == Stage.mercy_selection:
            selected_action = self.battle.interface.check_click()
            if selected_action == SelectedAction.press_back_to_action_selection:
                self.battle.start_action_selection()
            elif selected_action == SelectedAction.press_run_away:
                self.status = GameStatus.moving
            for event in pygame.event.get():
                self.handle_event_while_battle_selection(event)

        # Победа в бою
        elif self.battle.stage == Stage.win:
            self.level.remove_npc()
            self.status = GameStatus.moving

        # Поражение в бою
        elif self.battle.stage == Stage.lose:
            self.main_menu = MainMenu(self.screen, GameResult.lose)
            self.status = GameStatus.main_menu

    def handle_event_while_battle_selection(self, event):
        self.check_pause_and_exit(event)
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                self.battle.interface.click = True

    def handle_event_while_battle_fighting(self, event):
        self.check_pause_and_exit(event)
        if event.type == pygame.KEYDOWN:
            key = event.key
            # Начинаем передвижение игрока
            if key == pygame.K_w or key == pygame.K_UP:
                self.battle.player_heart.change_move_to(Direction.up)
            elif key == pygame.K_a or key == pygame.K_LEFT:
                self.battle.player_heart.change_move_to(Direction.left)
            elif key == pygame.K_s or key == pygame.K_DOWN:
                self.battle.player_heart.change_move_to(Direction.down)
            elif key == pygame.K_d or key == pygame.K_RIGHT:
                self.battle.player_heart.change_move_to(Direction.right)

        if event.type == pygame.KEYUP:
            key = event.key
            # Останавливаем передвижение игрока
            if key == pygame.K_w or key == pygame.K_UP:
                self.battle.player_heart.change_move_to(Direction.up)
            elif key == pygame.K_a or key == pygame.K_LEFT:
                self.battle.player_heart.change_move_to(Direction.left)
            elif key == pygame.K_s or key == pygame.K_DOWN:
                self.battle.player_heart.change_move_to(Direction.down)
            elif key == pygame.K_d or key == pygame.K_RIGHT:
                self.battle.player_heart.change_move_to(Direction.right)

    def perform_dialog(self):
        self.dialog_window.start_dialog()

        for event in pygame.event.get():
            self.handle_event_while_dialog(event)

    def handle_event_while_dialog(self, event):
        self.check_pause_and_exit(event)
        if event.type == pygame.KEYDOWN:
            key = event.key

            if key == pygame.K_RETURN:
                result = self.dialog_window.continue_dialog()
                if not result:
                    self.status = GameStatus.moving
