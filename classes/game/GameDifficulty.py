from enum import Enum


class GameDifficulty(Enum):
    easy = 8
    deadly = 5
