from enum import Enum


class GameResult(Enum):
    win = 1
    lose = 2
