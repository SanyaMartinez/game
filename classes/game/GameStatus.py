from enum import Enum


class GameStatus(Enum):
    main_menu = 1
    pause_menu = 2
    moving = 3
    battle = 4
    dialog = 5
    inventory = 6
