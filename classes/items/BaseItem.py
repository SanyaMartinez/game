from classes.BaseObject import BaseObject


class BaseItem(BaseObject):
    # Описание
    description: str

    # Величина значения при использовании предмета
    effect_value: int

    def __init__(self, name, image, coordinates, size, effect_value, description):
        super().__init__(name, image, coordinates, size)
        self.effect_value = effect_value
        self.description = description
