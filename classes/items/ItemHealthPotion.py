import pygame

from classes.items.BaseItem import BaseItem


class ItemHealthPotion(BaseItem):
    def __init__(self):
        name = "Зелье здоровья"
        image = pygame.image.load("assets/items/health_potion.png")
        coordinates = (0, 0)
        size = image.get_rect().size
        effect_value = 1
        description = "Восстанавливает {} ед. здоровья".format(effect_value)
        super().__init__(name, image, coordinates, size, effect_value, description)
