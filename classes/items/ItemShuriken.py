import pygame

from classes.items.BaseItem import BaseItem


class ItemShuriken(BaseItem):
    def __init__(self):
        name = "Сюрикен"
        image = pygame.image.load("assets/items/shuriken.png")
        coordinates = (0, 0)
        size = image.get_rect().size
        effect_value = 2
        description = "Бросить во врага и нанести ему {} ед. урона".format(effect_value)
        super().__init__(name, image, coordinates, size, effect_value, description)
