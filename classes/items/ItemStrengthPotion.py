import pygame

from classes.items.BaseItem import BaseItem


class ItemStrengthPotion(BaseItem):
    def __init__(self):
        name = "Зелье силы"
        image = pygame.image.load("assets/items/strength_potion.png")
        coordinates = (0, 0)
        size = image.get_rect().size
        effect_value = 1
        description = "Навсегда добавляет {} ед. к урону".format(effect_value)
        super().__init__(name, image, coordinates, size, effect_value, description)
