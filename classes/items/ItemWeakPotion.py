import pygame

from classes.items.BaseItem import BaseItem


class ItemWeakPotion(BaseItem):
    def __init__(self):
        name = "Зелье слабости"
        image = pygame.image.load("assets/items/weak_potion.png")
        coordinates = (0, 0)
        size = image.get_rect().size
        effect_value = 1
        description = "Бросить во врага и снизить его урон на {} ед.".format(effect_value)
        super().__init__(name, image, coordinates, size, effect_value, description)
