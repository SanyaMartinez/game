import pygame
from constants import *
from classes.player.Player import Player
from constants import CAMERA_START_COORDINATES


class Level:
    # Основные параметры для инициализации уровня
    number: int
    name: str
    player_start_coordinates: tuple
    bg_asset: pygame.image

    group_of_objects: pygame.sprite.Group
    group_of_characters: pygame.sprite.Group
    group_of_items: pygame.sprite.Group
    group_of_transitions: pygame.sprite.Group

    # Дополнительные параметры
    camera_pos: tuple
    background: pygame.image
    surface: pygame.Surface
    player: Player

    def __init__(self, number, name, bg_asset, objects, characters, items, transitions):
        self.number = number
        self.name = name
        self.bg_asset = bg_asset
        self.group_of_items = items
        self.group_of_objects = objects
        self.group_of_characters = characters
        self.group_of_transitions = transitions

    # Инициализация дополнительных параметров, начало уровня
    def start_level(self, player_info, player_start_coordinates):
        self.player_start_coordinates = player_start_coordinates
        self.camera_pos = self.get_camera_start_position()
        self.background = pygame.image.load(self.bg_asset).convert()
        self.surface = pygame.Surface(self.background.get_rect().size)
        self.create_player(player_info)

    # Возвращает начальные координаты камеры (центр экрана - стартовые координаты игрока)
    def get_camera_start_position(self):
        return tuple(map(sum, zip(CAMERA_START_COORDINATES, map(lambda x: x * -1, self.player_start_coordinates))))

    # Создаем игрока
    def create_player(self, player_info):
        name = player_info.name
        image = pygame.image.load(PLAYER_SKIN_MOVE_DOWN).convert_alpha()
        size = (PLAYER_WIDTH, PLAYER_HEIGHT)
        self.player = Player(name, image, self.player_start_coordinates, size, player_info)

    # Обновление позиции игрока и камеры (движение)
    def update_position(self):
        all_objects = self.group_of_objects.copy()
        all_objects.add(self.group_of_characters.copy())
        self.camera_pos = self.player.move(all_objects, self.camera_pos)

    # Отрисовка фона, персонажа, объектов
    def draw(self):
        self.surface.blit(self.background, (0, 0))
        self.group_of_characters.draw(self.surface)
        self.group_of_transitions.draw(self.surface)
        self.group_of_items.draw(self.surface)
        self.surface.blit(self.player.image, self.player.rect)

    # Проверить наличие NPC рядом
    def check_npc_nearby(self):
        for npc in self.group_of_characters:
            w_diff = abs(self.player.rect.x - npc.rect.x)
            h_diff = abs(self.player.rect.y - npc.rect.y)
            if w_diff + h_diff < MIN_DISTANCE_TO_CHECK:
                return npc

    def remove_npc(self):
        npc = self.check_npc_nearby()
        if npc is not None:
            self.drop_loot(npc)
            self.group_of_characters.remove(npc)

    def drop_loot(self, npc):
        loot = npc.loot
        if loot is not None:
            loot.rect.center = npc.rect.center
            loot.rect.top = npc.rect.bottom
            self.group_of_items.add(loot)

    # Проверить наличие лута рядом
    def check_loot_nearby(self):
        for loot in self.group_of_items:
            w_diff = abs(self.player.rect.x - loot.rect.x)
            h_diff = abs(self.player.rect.y - loot.rect.y)
            if w_diff + h_diff < MIN_DISTANCE_TO_CHECK:
                self.get_loot(loot)
                return

    # подобрать лут
    def get_loot(self, loot):
        self.player.player_info.inventory.add(loot)
        self.group_of_items.remove(loot)

    def check_level_transition(self):
        for transition in self.group_of_transitions:
            if self.player.rect.colliderect(transition):
                return transition.next_level_number, transition.next_level_player_start_coordinates
        return None, None
