import pygame

from classes.BaseObject import BaseObject


# Переход между уровнями
class Transition(BaseObject):
    next_level_number: int
    next_level_player_start_coordinates: tuple

    def __init__(self, coordinates, next_level_number, next_level_player_start_coordinates):
        name = "Переход"
        image = pygame.image.load("assets/levels/transition.png")
        size = image.get_rect().size
        super().__init__(name, image, coordinates, size)
        self.next_level_number = next_level_number
        self.next_level_player_start_coordinates = next_level_player_start_coordinates
