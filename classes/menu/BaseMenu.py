import pygame

from classes.RectObject import RectObject
from constants import SCREEN_BLACK, MENU_BG_COORDINATES, MENU_BG_SIZE


class BaseMenu:
    font: pygame.font
    title_text: str

    title: pygame.Surface
    buttons: pygame.sprite.Group

    surface: pygame.Surface
    click: bool

    def __init__(self, surface):
        self.font = pygame.font.SysFont(None, 30)
        self.surface = surface
        self.click = False
        self.bg = RectObject(MENU_BG_COORDINATES, MENU_BG_SIZE)
        self.bg.rect.center = MENU_BG_COORDINATES

    def set_screen(self, title_text, buttons):
        self.title_text = title_text
        self.buttons = buttons

    @staticmethod
    def create_buttons(buttons_list):
        buttons = pygame.sprite.Group()
        for button in buttons_list:
            buttons.add(button)
        return buttons

    def draw(self):
        self.draw_bg()
        self.draw_title()
        self.draw_buttons()

    def draw_bg(self):
        pygame.draw.rect(self.surface, SCREEN_BLACK, self.bg.rect)

    def draw_title(self):
        self.title = self.font.render(self.title_text, True, (255, 255, 255))
        title_rect = self.title.get_rect()
        title_rect.midtop = self.bg.rect.midtop
        title_rect.y += 30
        self.surface.blit(self.title, title_rect)

    def draw_buttons(self):
        for button in self.buttons:
            # Фон кнопки
            pygame.draw.rect(self.surface, (255, 255, 255), button)
            # Текст кнопки
            title_rect = button.title.get_rect()
            title_rect.center = button.rect.center
            self.surface.blit(button.title, title_rect)

    def check_click(self):
        mouse_pos = pygame.mouse.get_pos()

        for button in self.buttons:
            if button.rect.collidepoint(mouse_pos):
                if self.click:
                    self.click = False
                    return button.on_click()

        self.click = False
