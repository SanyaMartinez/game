from classes.game.GameDifficulty import GameDifficulty
from classes.game.GameResult import GameResult
from classes.menu.BaseMenu import BaseMenu
from classes.Button import Button
from constants import MENU_BUTTON_COORDINATES_2, MENU_BUTTON_COORDINATES_1, MENU_BUTTON_SIZE


class MainMenu(BaseMenu):
    start: bool
    difficulty: GameDifficulty

    exit: bool

    def __init__(self, surface, game_result=None):
        super().__init__(surface)
        self.start = False
        self.exit = False
        title = "Главное меню" if game_result is None else "Вы выиграли" if game_result == GameResult.win else "Вы проиграли"
        self.set_screen(title, self.get_buttons_choose_option())

    def get_buttons_choose_option(self):
        button1 = Button(MENU_BUTTON_COORDINATES_1, MENU_BUTTON_SIZE, "Начать игру", self.set_screen,
                             "Выберите сложность", self.get_buttons_choose_game_difficulty())
        button1.rect.center = MENU_BUTTON_COORDINATES_1

        button2 = Button(MENU_BUTTON_COORDINATES_2, MENU_BUTTON_SIZE, "Выйти", self.exit_from_game)
        button2.rect.center = MENU_BUTTON_COORDINATES_2
        return self.create_buttons([button1, button2])

    def get_buttons_choose_game_difficulty(self):
        button1 = Button(MENU_BUTTON_COORDINATES_1, MENU_BUTTON_SIZE, "Легко", self.start_game,
                         GameDifficulty.easy)
        button1.rect.center = MENU_BUTTON_COORDINATES_1

        button2 = Button(MENU_BUTTON_COORDINATES_2, MENU_BUTTON_SIZE, "Смертельно", self.start_game,
                         GameDifficulty.deadly)
        button2.rect.center = MENU_BUTTON_COORDINATES_2
        return self.create_buttons([button1, button2])

    def start_game(self, difficulty):
        self.start = True
        self.difficulty = difficulty

    def exit_from_game(self):
        self.exit = True
