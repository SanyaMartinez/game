from classes.menu.BaseMenu import BaseMenu
from classes.Button import Button
from constants import MENU_BUTTON_COORDINATES_1, MENU_BUTTON_SIZE, MENU_BUTTON_COORDINATES_2


class PauseMenu(BaseMenu):
    resume: bool
    exit: bool

    def __init__(self, surface):
        super().__init__(surface)
        self.resume = False
        self.exit = False
        self.set_screen("Пауза", self.get_buttons_choose_option())

    def get_buttons_choose_option(self):
        button1 = Button(MENU_BUTTON_COORDINATES_1, MENU_BUTTON_SIZE, "Продолжить", self.resume_game)
        button1.rect.center = MENU_BUTTON_COORDINATES_1

        button2 = Button(MENU_BUTTON_COORDINATES_2, MENU_BUTTON_SIZE, "Выйти", self.exit_from_game)
        button2.rect.center = MENU_BUTTON_COORDINATES_2
        return self.create_buttons([button1, button2])

    def resume_game(self):
        self.resume = True

    def exit_from_game(self):
        self.exit = True
