from classes.BaseObject import BaseObject
from classes.items.BaseItem import BaseItem
from classes.npc.NPCInfo import NPCInfo


class NPC(BaseObject):
    # Идентификатор для связи диалогов и боев с NPC
    number: int
    # Диалоги
    font_name: str
    dialog_texts: list
    # Общая инфа в сражении
    npc_info: NPCInfo
    # лут
    loot: BaseItem

    def __init__(self, number, name, image, coordinates, size, font_name, dialog_texts, npc_info, loot):
        super().__init__(name, image, coordinates, size)
        self.number = number
        self.font_name = font_name
        self.dialog_texts = dialog_texts
        self.npc_info = npc_info
        self.loot = loot
