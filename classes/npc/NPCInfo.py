# Общая Информация о NPC
class NPCInfo:
    # Характеристики
    name: str
    max_health: int
    current_health: int
    damage: int

    def __init__(self, name, max_health, damage):
        self.name = name
        self.max_health = max_health
        self.current_health = max_health
        self.damage = damage
