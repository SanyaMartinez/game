from classes.BaseObject import BaseObject
from classes.Direction import Direction
from classes.player.PlayerInfo import PlayerInfo
from constants import *
import pygame


# Игрок (на уровне)
class Player(BaseObject):
    # Общая инфа
    player_info: PlayerInfo
    # Передвижение
    move_up: bool
    move_left: bool
    move_down: bool
    move_right: bool
    # Координаты камеры
    camera: pygame.math.Vector2

    def __init__(self, name, image, start_coordinates, size, player_info):
        super().__init__(name, image, start_coordinates, size)
        self.player_info = player_info
        self.move_up = False
        self.move_left = False
        self.move_down = False
        self.move_right = False
        self.camera = pygame.math.Vector2()

    def move(self, objects, old_camera_pos):
        # Текущие координаты камеры
        pos_x, pos_y = old_camera_pos

        # Перемещение игрока и камеры (противоположно) по оси X
        if self.move_left:
            self.rect.x -= PLAYER_SPEED
            pos_x += PLAYER_SPEED
        if self.move_right:
            self.rect.x += PLAYER_SPEED
            pos_x -= PLAYER_SPEED

        # Обрабатываем столкновения с объектами по оси X
        for single_object in objects:
            if self.rect.colliderect(single_object.rect):
                if self.move_right:
                    self.rect.right = single_object.rect.left
                    pos_x = old_camera_pos[0]
                elif self.move_left:
                    self.rect.left = single_object.rect.right
                    pos_x = old_camera_pos[0]

        # Перемещение игрока и камеры (противоположно) по оси Y
        if self.move_up:
            self.rect.y -= PLAYER_SPEED
            pos_y += PLAYER_SPEED
        if self.move_down:
            self.rect.y += PLAYER_SPEED
            pos_y -= PLAYER_SPEED

        # Обрабатываем столкновения с объектами по оси Y
        for single_object in objects:
            if self.rect.colliderect(single_object.rect):
                if self.move_up:
                    self.rect.top = single_object.rect.bottom
                    pos_y = old_camera_pos[1]
                elif self.move_down:
                    self.rect.bottom = single_object.rect.top
                    pos_y = old_camera_pos[1]

        # Смена спрайта - анимация
        if self.move_up:
            image = PLAYER_SKIN_MOVE_UP
        elif self.move_down:
            image = PLAYER_SKIN_MOVE_DOWN
        elif self.move_left:
            image = PLAYER_SKIN_MOVE_LEFT
        elif self.move_right:
            image = PLAYER_SKIN_MOVE_RIGHT
        else:
            image = PLAYER_SKIN_MOVE_DOWN
        self.image = pygame.image.load(image).convert_alpha()

        # Возвращаем новые координаты камеры
        return pos_x, pos_y

    def change_move_to(self, direction):
        if direction == Direction.up:
            self.move_up = not self.move_up
        elif direction == Direction.left:
            self.move_left = not self.move_left
        elif direction == Direction.down:
            self.move_down = not self.move_down
        elif direction == Direction.right:
            self.move_right = not self.move_right

    def stop_move(self):
        self.move_up = False
        self.move_left = False
        self.move_down = False
        self.move_right = False
