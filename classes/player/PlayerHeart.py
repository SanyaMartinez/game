from classes.BaseObject import BaseObject
from classes.Direction import Direction
from classes.player.PlayerInfo import PlayerInfo
from constants import *


# Игрок во время боя
class PlayerHeart(BaseObject):
    # Общая инфа
    player_info: PlayerInfo
    # Передвижение
    move_up: bool
    move_left: bool
    move_down: bool
    move_right: bool

    def __init__(self, name, image, start_coordinates, size, player_info):
        super().__init__(name, image, start_coordinates, size)
        self.player_info = player_info
        self.move_up = False
        self.move_left = False
        self.move_down = False
        self.move_right = False

    def move(self):
        # Перемещение игрока по оси X
        if self.move_left and (self.rect.x - PLAYER_HEART_SPEED) > FIGHT_BORDER_X_LEFT:
            self.rect.x -= PLAYER_HEART_SPEED
        if self.move_right and (self.rect.x + PLAYER_HEART_SPEED) < FIGHT_BORDER_X_RIGHT:
            self.rect.x += PLAYER_HEART_SPEED

        # Перемещение игрока по оси Y
        if self.move_up and (self.rect.y - PLAYER_HEART_SPEED) > FIGHT_BORDER_Y_TOP:
            self.rect.y -= PLAYER_HEART_SPEED
        if self.move_down and (self.rect.y + PLAYER_HEART_SPEED) < FIGHT_BORDER_Y_BOTTOM:
            self.rect.y += PLAYER_HEART_SPEED

    def change_move_to(self, direction):
        if direction == Direction.up:
            self.move_up = not self.move_up
        elif direction == Direction.left:
            self.move_left = not self.move_left
        elif direction == Direction.down:
            self.move_down = not self.move_down
        elif direction == Direction.right:
            self.move_right = not self.move_right

    def check_damage_taken(self, objects):
        # Обрабатываем столкновения с объектами
        for single_object in objects:
            if self.rect.colliderect(single_object.rect):
                return True

        return False
