# Общая Информация об игроке
import pygame

from classes.items.ItemHealthPotion import ItemHealthPotion
from classes.items.ItemWeakPotion import ItemWeakPotion
from classes.items.ItemShuriken import ItemShuriken
from classes.items.ItemStrengthPotion import ItemStrengthPotion


class PlayerInfo:
    # Характеристики
    name: str
    max_health: int
    current_health: int
    damage: int
    inventory: pygame.sprite.Group

    def __init__(self, name, max_health):
        self.name = name
        self.max_health = max_health
        self.current_health = max_health
        self.damage = 1
        self.inventory = pygame.sprite.Group()
        # TODO remove
        self.inventory.add(ItemHealthPotion())
        self.inventory.add(ItemHealthPotion())
        self.inventory.add(ItemHealthPotion())
        self.inventory.add(ItemStrengthPotion())
        self.inventory.add(ItemStrengthPotion())
        self.inventory.add(ItemStrengthPotion())
        self.inventory.add(ItemShuriken())
        self.inventory.add(ItemShuriken())
        self.inventory.add(ItemWeakPotion())
