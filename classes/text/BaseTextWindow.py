import pygame

from classes.RectObject import RectObject
from constants import *


class BaseTextWindow(RectObject):
    # Экран для отрисовки
    surface: pygame.Surface

    font: pygame.font

    def __init__(self, coordinates, size, surface):
        super().__init__(coordinates, size)
        self.surface = surface
        self.font = pygame.font.SysFont(None, 30)

    # отрисовка текста c отступами
    def blit_text(self, text):
        words = [word.split(' ') for word in text.splitlines()]  # 2D array where each row is a list of words.
        space = self.font.size(' ')[0]  # The width of a space.

        pos = (self.rect.x + 20, self.rect.y + 20)
        x, y = pos
        max_width, max_height = (self.rect.width - 20, self.rect.height - 20)
        word_width, word_height = 0, 0

        pygame.draw.rect(self.surface, SCREEN_BLACK, self)  # Рисуем черный фон
        for line in words:
            for word in line:
                word_surface = self.font.render(word, True, TEXT_COLOR_WHITE)
                word_width, word_height = word_surface.get_size()
                if x + word_width >= pos[0] + max_width:
                    x = pos[0]  # Reset the x.
                    y += word_height  # Start on new row.
                self.surface.blit(word_surface, (x, y))
                x += word_width + space
            x = pos[0]  # Reset the x.
            y += word_height  # Start on new row.
