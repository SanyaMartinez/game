from classes.text.BaseTextWindow import BaseTextWindow, BATTLE_TEXT_WINDOW_COORDINATES, BATTLE_TEXT_WINDOW_SIZE


class BattleTextWindow(BaseTextWindow):

    def __init__(self, surface):
        super().__init__(BATTLE_TEXT_WINDOW_COORDINATES, BATTLE_TEXT_WINDOW_SIZE, surface)
