import pygame

from classes.text.BaseTextWindow import BaseTextWindow, DIALOG_WINDOW_COORDINATES, DIALOG_WINDOW_SIZE


class DialogWindow(BaseTextWindow):

    # Имена игрока и собеседника
    player_name: str
    npc_name: str

    dialog_texts: list
    current_text: int

    def __init__(self, surface):
        super().__init__(DIALOG_WINDOW_COORDINATES, DIALOG_WINDOW_SIZE, surface)
        self.rect.center = DIALOG_WINDOW_COORDINATES

    def set_player_name(self, player_name):
        self.player_name = player_name

    def set_dialog_preferences(self, font_name, texts, npc_name):
        self.font = pygame.font.SysFont(font_name, 20)
        self.dialog_texts = texts
        self.current_text = 0
        self.npc_name = npc_name

    def start_dialog(self):
        if self.current_text < len(self.dialog_texts):
            text = self.edit_text(self.dialog_texts[self.current_text])
            self.blit_text(text)
            return True
        else:
            return False

    def continue_dialog(self):
        self.current_text += 1
        return self.start_dialog()

    def edit_text(self, text_info):
        speaker, text = text_info
        return (self.player_name if speaker == 1 else self.npc_name) + ":\n\n" + '"' + text + '"'
