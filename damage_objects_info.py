import random

import pygame

from classes.battle.damage_objects.Explosion import Explosion
from classes.battle.damage_objects.Katana import Katana
from classes.battle.damage_objects.RandomSnowball import RandomSnowball
from classes.battle.damage_objects.Shuriken import Shuriken
from classes.battle.damage_objects.Spider import Spider
from constants import SCREEN_WIDTH, SCREEN_HEIGHT, FIGHT_BORDER_Y_TOP, FIGHT_BORDER_Y_BOTTOM


def create_battle_damage_objects(npc_number):
    if npc_number == 0:
        return create_damage_objects_random_snowball()
    elif npc_number == 1:
        return create_damage_objects_shuriken()
    elif npc_number == 2:
        return create_damage_objects_katana()
    elif npc_number == 3:
        return create_damage_objects_explosion()
    elif npc_number == 4:
        return create_damage_objects_spider()


def create_damage_objects_random_snowball():
    damage_objects = pygame.sprite.Group()

    name = "snowball"
    image = pygame.image.load("assets/battle/damage_objects/snowball.png")
    size = image.get_rect().size
    speed = 3
    for i in range(1, 4):
        coordinates = (SCREEN_WIDTH / 2 - 300, SCREEN_HEIGHT / 2 + 50 * i)
        snowball = RandomSnowball(name, image, coordinates, size, speed)
        damage_objects.add(snowball)
    for i in range(1, 4):
        coordinates = (SCREEN_WIDTH / 2 - 100, SCREEN_HEIGHT / 2 + 50 * i)
        snowball = RandomSnowball(name, image, coordinates, size, speed)
        damage_objects.add(snowball)
    for i in range(1, 4):
        coordinates = (SCREEN_WIDTH / 2 + 100, SCREEN_HEIGHT / 2 + 50 * i)
        snowball = RandomSnowball(name, image, coordinates, size, speed)
        damage_objects.add(snowball)
    for i in range(1, 4):
        coordinates = (SCREEN_WIDTH / 2 + 300, SCREEN_HEIGHT / 2 + 50 * i)
        snowball = RandomSnowball(name, image, coordinates, size, speed)
        damage_objects.add(snowball)

    return damage_objects


def create_damage_objects_shuriken():
    damage_objects = pygame.sprite.Group()

    name = "shuriken"
    image = pygame.image.load("assets/battle/damage_objects/shuriken.png")
    size = image.get_rect().size
    speed = 1
    coordinates = [
        (SCREEN_WIDTH / 2 - 300, SCREEN_HEIGHT / 2 + 50),
        (SCREEN_WIDTH / 2 + 300, SCREEN_HEIGHT / 2 + 50),
        (SCREEN_WIDTH / 2 - 300, SCREEN_HEIGHT / 2 + 150),
        (SCREEN_WIDTH / 2 + 300, SCREEN_HEIGHT / 2 + 150)
    ]
    for i in range(4):
        shuriken = Shuriken(name, image, coordinates[i], size, speed)
        damage_objects.add(shuriken)

    return damage_objects


def create_damage_objects_spider():
    damage_objects = pygame.sprite.Group()

    name = "spider"
    image = pygame.image.load("assets/battle/damage_objects/spider.png")
    size = image.get_rect().size
    for i in range(1, 70):
        speed = random.randint(5, 7)
        coordinates = (random.randint(0, SCREEN_WIDTH), 0 - 50 * i)
        spider = Spider(name, image, coordinates, size, speed)
        damage_objects.add(spider)

    return damage_objects


def create_damage_objects_explosion():
    damage_objects = pygame.sprite.Group()

    name = "explosion"
    image = pygame.image.load("assets/battle/damage_objects/explosion.png")
    size = (0, 0)
    speed = 3

    for i in range(3):
        coordinates = (0, 0)
        explosion = Explosion(name, image, coordinates, size, speed, 30 + 30 * i)
        damage_objects.add(explosion)

    return damage_objects


def create_damage_objects_katana():
    damage_objects = pygame.sprite.Group()

    name = "katana"
    image1 = pygame.image.load("assets/battle/damage_objects/katana.png")
    image2 = pygame.transform.flip(image1, False, True)
    image3 = pygame.transform.flip(image1, True, False)
    image4 = pygame.transform.flip(image1, True, True)
    size = image1.get_rect().size
    for i in range(8):
        y_save = random.randint(FIGHT_BORDER_Y_TOP + 10, FIGHT_BORDER_Y_BOTTOM - 10)

        speed = 3
        coordinates = (0 - 600 * i, 0)
        katana1 = Katana(name, image1, coordinates, size, speed)
        katana2 = Katana(name, image2, coordinates, size, speed)

        katana1.rect.top = y_save + 60
        katana2.rect.bottom = y_save

        damage_objects.add(katana1)
        damage_objects.add(katana2)

    for i in range(8):
        y_save = random.randint(FIGHT_BORDER_Y_TOP + 10, FIGHT_BORDER_Y_BOTTOM - 10)

        speed = -3
        coordinates = (1500 + 800 * i, 0)
        katana3 = Katana(name, image3, coordinates, size, speed)
        katana4 = Katana(name, image4, coordinates, size, speed)

        katana3.rect.top = y_save + 60
        katana4.rect.bottom = y_save

        damage_objects.add(katana3)
        damage_objects.add(katana4)

    return damage_objects
