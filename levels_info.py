import pygame

from NPCs_info import NPC_NINJA_KATANA, NPC_NINJA_SHURIKEN, NPC_PIRATE_CAPTAIN
from classes.level.Level import Level
from classes.npc.NPC import NPC
from classes.RectObject import RectObject
from classes.level.Transition import Transition
from classes.npc.NPCInfo import NPCInfo
from constants import PLAYER_HEIGHT
from dialogs_info import ALL_DIALOGS


def create_level(number, name, bg_asset, objects_info, NPCs_info, items_info, transitions_info):
    # Создаем объекты (препятствия) из списка
    objects = pygame.sprite.Group()
    obj_list = map(lambda x: RectObject((x[0], x[1]), (x[2], x[3])), objects_info)
    for obj in obj_list:
        objects.add(obj)
    for rect_object in objects:
        rect_object.rect.height -= PLAYER_HEIGHT - 10

    # Создаем NPC из списка
    NPCs = pygame.sprite.Group()
    NPCs_list = map(lambda x: create_npc(x[0], x[1]), NPCs_info)
    for char in NPCs_list:
        NPCs.add(char)
    for rect_object in NPCs:
        rect_object.rect.height -= PLAYER_HEIGHT - 10

    # TODO: Создаем предметы инвентаря из списка
    items = pygame.sprite.Group()
    items_list = map(lambda x: create_item(x[0], x[1]), items_info)
    for item in items_list:
        items.add(item)

    # Создаем переходы на другие уровни
    transitions = pygame.sprite.Group()
    transitions_list = map(lambda x: Transition(x[0], x[1], x[2]), transitions_info)
    for item in transitions_list:
        transitions.add(item)

    return Level(number, name, bg_asset, objects, NPCs, items, transitions)


def create_item(item, coordinates):
    item.rect.x, item.rect.y = coordinates
    return item


def create_npc(npc, coordinates):
    number, name, image_asset, font_name, max_health, damage, loot = npc
    image = pygame.image.load(image_asset)
    size = image.get_rect().size
    dialog_texts = ALL_DIALOGS[number] if number in ALL_DIALOGS.keys() else None
    npc_info = NPCInfo(name, max_health, damage)
    return NPC(number, name, image, coordinates, size, font_name, dialog_texts, npc_info, loot)


LVL_0 = create_level(0, "test", "assets/levels/test_1.jpg",
                     [
                         (0, 0, 580, 208),
                         (580, 0, 194, 348),
                         (770, 0, 430, 255),
                         (0, 0, 8, 638),
                         (0, 638, 245, 36),
                         (340, 638, 244, 38),
                         (580, 496, 194, 180),
                         (774, 590, 438, 22),
                         (1204, 0, 8, 612),
                         (0, 0, 292, 254),
                         (342, 210, 48, 24),
                         (530, 198, 50, 62),
                         (776, 258, 37, 49),
                         (870, 258, 46, 48),
                         (920, 258, 142, 142),
                         (1110, 258, 95, 26),
                         (1114, 550, 90, 40),
                         (776, 546, 46, 44),
                         (490, 596, 90, 42),
                         (536, 550, 42, 40),
                         (0, 558, 186, 72)
                     ],
                     [
                         # NPC_NINJA_SHURIKEN,
                         # NPC_NINJA_KATANA
                     ],
                     [
                         # items
                     ],
                     [
                         # transitions
                         ((450, 450), 1, (380, 730))
                     ])

LVL_1 = create_level(1, "шалаши", "assets/levels/maps1.png",
                     [
                         # objects
                     ],
                     [
                         (NPC_PIRATE_CAPTAIN, (1060, 400)),
                         (NPC_NINJA_KATANA, (1540, 1190))
                     ],
                     [
                         # items
                         # (ItemShuriken(), (1000, 600))
                     ],
                     [
                         # transitions
                         ((3980, 748), 2, (134, 642)),
                         ((1088, 162), 5, (1701, 3219)),
                         ((321, 740), 15, (2871, 1183))
                     ])
LVL_2 = create_level(2, "тропа", "assets/levels/maps2.png",
                     [
                         # objects
                     ],
                     [
                         # NPCs
                     ],
                     [
                         # items
                     ],
                     [
                         # transitions
                         ((108, 657), 1, (3980, 720)),
                         ((2385, 610), 3, (152, 904))
                     ])
LVL_3 = create_level(3, "деревня", "assets/levels/maps3.png",
                     [
                         # objects
                     ],
                     [
                         # NPCs
                     ],
                     [
                         # items
                     ],
                     [
                         # transitions
                         ((150, 910), 2, (2388, 578)),
                         ((1146, 1888), 4, (1184, 300)),
                         ((575, 737), 8, (312, 662)),
                         ((1531, 737), 9, (312, 662)),
                         ((290, 1268), 10, (312, 662)),
                         ((1971, 1278), 11, (312, 662)),
                         ((1580, 1306), 14, (510, 752))
                     ])
LVL_4 = create_level(4, "путь к боссу", "assets/levels/maps4.png",
                     [
                         # objects
                     ],
                     [
                         # NPCs
                     ],
                     [
                         # items
                     ],
                     [
                         # transitions
                         ((1180, 266), 3, (1146, 1848)),
                         ((2186, 824), 13, (1060, 1660))
                     ])
LVL_5 = create_level(5, "пляж", "assets/levels/maps5.png",
                     [
                         # objects
                     ],
                     [
                         # NPCs
                     ],
                     [
                         # items
                     ],
                     [
                         # transitions
                         ((1706, 3278), 1, (1088, 162)),
                         ((1314, 1972), 6, (422, 289)),
                         ((2169, 1820), 7, (1378, 312)),
                     ])
LVL_6 = create_level(6, "корабль первыя палуба", "assets/levels/maps5_1.png",
                     [
                         # objects
                     ],
                     [
                         # NPCs
                     ],
                     [
                         # items
                     ],
                     [
                         # transitions
                         ((430, 305), 5, (1364, 1966))
                     ])
LVL_7 = create_level(7, "корабль нижняя палуба", "assets/levels/maps5_2.png",
                     [
                         # objects
                     ],
                     [
                         # NPCs
                     ],
                     [
                         # items
                     ],
                     [
                         # transitions
                         ((1380, 303), 5, (2158, 1801))
                     ])
LVL_8 = create_level(8, "дом 1", "assets/levels/home_1.png",
                     [
                         # objects
                     ],
                     [
                         # NPCs
                     ],
                     [
                         # items
                     ],
                     [
                         # transitions
                         ((312, 662), 3, (585, 747))
                     ])
LVL_9 = create_level(9, "дом 2", "assets/levels/home_2.png",
                     [
                         # objects
                     ],
                     [
                         # NPCs
                     ],
                     [
                         # items
                     ],
                     [
                         # transitions
                         ((312, 662), 3, (1541, 747)),
                         ((705, 363), 12, (1711, 530))
                     ])
LVL_10 = create_level(10, "дом 3", "assets/levels/home_3.png",
                      [
                          # objects
                      ],
                      [
                          # NPCs
                      ],
                      [
                          # items
                      ],
                      [
                          # transitions
                          ((312, 662), 3, (309, 1278))
                      ])
LVL_11 = create_level(11, "дом 4", "assets/levels/home_4.png",
                      [
                          # objects
                      ],
                      [
                          # NPCs
                      ],
                      [
                          # items
                      ],
                      [
                          # transitions
                          ((312, 662), 3, (1971, 1278))
                      ])
LVL_12 = create_level(9, "канализация", "assets/levels/ditch.png",
                      [
                          # objects
                      ],
                      [
                          # NPCs
                      ],
                      [
                          # items
                      ],
                      [
                          # transitions
                          ((1710, 550), 9, (711, 363))
                      ])
LVL_13 = create_level(8, "босс", "assets/levels/boss.png",
                      [
                          # objects
                      ],
                      [
                          # NPCs
                      ],
                      [
                          # items
                      ],
                      [
                          # transitions
                          ((1050, 1660), 4, (2186, 824))
                      ])
LVL_14 = create_level(8, "магаз", "assets/levels/shop.png",
                      [
                          # objects
                      ],
                      [
                          # NPCs
                      ],
                      [
                          # items
                      ],
                      [
                          # transitions
                          ((510, 752), 3, (1598, 1316))
                      ])
LVL_15 = create_level(8, "Старт", "assets/levels/start.png",
                      [
                          # objects
                      ],
                      [
                          # NPCs
                      ],
                      [
                          # items
                      ],
                      [
                          # transitions
                          ((2871, 1195), 1, (321, 740))
                      ])
ALL_LEVELS = [
    LVL_0,
    LVL_1,
    LVL_2,
    LVL_3,
    LVL_4,
    LVL_5,
    LVL_6,
    LVL_7,
    LVL_8,
    LVL_9,
    LVL_10,
    LVL_11,
    LVL_12,
    LVL_13,
    LVL_14,
    LVL_15
]
