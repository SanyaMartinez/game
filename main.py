from classes.game.Game import Game


def init_game():
    game = Game()
    game.play()


if __name__ == '__main__':
    init_game()
